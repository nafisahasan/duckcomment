/**
 * @class index
 * from here app execution starts
 */


/**
 * @method start
 * takes us to description page
 */
function start() {
	var winDesc = Alloy.createController('desc').getView().open();
	winDesc = null;
}

$.winIndex.open();
