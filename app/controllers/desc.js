/**
 * @class desc
 * it gives description of various tags
 */


/**
 *@method next
 * It opens home page
 */
function next() {
	var winHome = Alloy.createController('home').getView().open();
	winHome = null;
}

/**
 *@method cancelpage
 * It closes current window and opens index page
 */
function cancelpage() {
	$.winDesc.close();
	var winIndex = Alloy.createController('index').getView().open();
	winIndex = null;
}