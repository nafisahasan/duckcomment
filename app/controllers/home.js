/**
 * @class home
 * home page
 */


/**
 * @method change
 * print out the searchbar value in console whenever it changes
 * @param {Object} e
 */
function change(e) {
	console.log('user searching for: ' + e.value);
}

/**
 * @method cancel
 * when cancel button is tapped, removes focus from our searchBar
 */
function cancel() {
	$.searchBar.blur();
}

/**
 * @method cancelpage
 * takes the user to home page
 */
function cancelpage(e) {
	$.vwDescription.setVisible(false);
	var winHome = Alloy.createController('home').getView().open();
	winHome = null;
}

/**
 * The following code installs the database
 * Executes database query
 * creates row as per requirement and displays database values in it
 * pushes database column values in object array named "data"
 */
data = [],
i = 0;
var db = Ti.Database.install('Duck.db', 'Duck');
console.log("db =>" + JSON.stringify(db));
var rows = db.execute('SELECT tags,description,example,url FROM duckinfo');
console.log("rows =>" + JSON.stringify(rows));

//push the data to the object
while (rows.isValidRow()) {
	data.push({
		tags : rows.fieldByName('tags'),
		desc : rows.fieldByName('description'),
		ex : rows.fieldByName('example'),
		url : rows.fieldByName('url')
	});
	
	//creates row
	var row = Ti.UI.createTableViewRow({
		title : data[i].tags,
		hasChild : true,
	});
	Ti.API.info(data[i].tags + ' ' + data[i].desc + ' ' + data[i].ex + ' ');
	$.table.appendRow(row);
	i++;
	
	//gets next row from database
	rows.next();
}

/**
 * @method gotoindex
 * takes us to index page
 */
function gotoindex() {
	var winIndex = Alloy.createController('index').getView().open();
	winIndex = null;
}

/**
 *@method detail
 * @param {Object} e
 * OnClick event of table row this function is called to display row details
 */
function detail(e) {
	var i = e.index;
	$.vwDescription.setVisible(true);
	$.vwContainer.setVisible(false);
	$.lbltag.setText("Tag:-  " + data[i].tags);
	$.lbldesc.setText("Description: -  " + data[i].desc);
	$.lblex.setText("Example: -  " + data[i].ex);
}
